//UNCOMMENT THE PARTS YOU WANT TO PRINT
//LATCH_HANDLE();
//LATCH_HANDLE_PLATE(); //PRINT TWO OF THESE
//TOP_PLATE();
//INSIDE_PLATE();
///////////////////////////////////////////////

module LATCH_HANDLE(){
difference(){
    union(){
    translate([0,0,0]) cube(size = [21,160,10], center = true);
    translate([0,81.5,0]) cube(size = [17,5,10], center = true);
    mirror([0,1,0]) translate([0,81.5,0]) cube(size = [17,5,10], center = true);
    }


       translate ([0,77,0])rotate([90,0,0]) cylinder (r = 3, h=15,center = true,$fn=64);
           mirror([0,1,0]) translate ([0,77,0])rotate([90,0,0]) cylinder (r = 3, h=15,center = true,$fn=64);
}


difference(){
union(){
    translate([15,(4+(7/2)),5]) cube(size = [12,8,20], center = true);
   mirror([0,1,0]) translate([15,(4+(7/2)),5]) cube(size = [12,8,20], center = true);
}
     translate ([17,0,10]) rotate([90,0,0]) cylinder (r = 2.5, h=30,center = true,$fn=64);
     translate ([17,0,0]) rotate([90,0,0]) cylinder (r = 2.5, h=30,center = true,$fn=64);
}
}


module LATCH_HANDLE_PLATE(){
difference(){
translate([0,0,50]) cube(size = [25,5,10], center = true);

       translate ([0,0,50])rotate([90,0,0]) cylinder (r = 2.5, h=15,center = true,$fn=64);
}
}

module TOP_PLATE(){
    inside_bracket();
    difference(){
        translate([0,0,0]) cube(size = [80,80,5], center = true);
    mount_holes();
    mirror([0,1,0]) mount_holes();
    }

    difference(){
    union(){
        translate([34,(4+(9.5/2)),12.5]) cube(size = [12,8,20], center = true);
  mirror([0,1,0]) translate([34,(4+(9.5/2)),12.5]) cube(size = [12,8,20], center = true);
    }

        translate ([32.5,0,12+5]) rotate([90,0,0]) cylinder (r = 2.5, h=30,center = true,$fn=64);
}


}
    module mount_holes(){
    translate ([35,35,0])rotate([0,0,0]) cylinder (r = 2.5, h=15,center = true,$fn=64);
        translate ([35-30,35,0])rotate([0,0,0]) cylinder (r = 2.5, h=15,center = true,$fn=64);
            translate ([35-30-30,35,0])rotate([0,0,0]) cylinder (r = 2.5, h=15,center = true,$fn=64);
    }

     module inside_mount_holes(){
    translate ([35,35,0])rotate([0,0,0]) cylinder (r = 2, h=15,center = true,$fn=64);
        translate ([35-30,35,0])rotate([0,0,0]) cylinder (r = 2, h=15,center = true,$fn=64);
            translate ([35-30-30,35,0])rotate([0,0,0]) cylinder (r = 2, h=15,center = true,$fn=64);
    }



    module INSIDE_PLATE(){
   difference(){
        translate([0,0,0]) cube(size = [80,80,5], center = true);
    inside_mount_holes();
    mirror([0,1,0]) inside_mount_holes();
    }





}
