import pifacedigitalio as p
import time
from subprocess import call


def toggle_led0(event):
    event.chip.leds[0].toggle()


def toggle_led1(event):
    event.chip.leds[1].toggle()


def toggle_led2(event):
    event.chip.leds[2].toggle()


def open(event):
    toggle_led0(event)
    p.digital_write(1, 1)
    p.digital_write(0, 0)
    time.sleep(2)
    releaseall(event)


def close(event):
    toggle_led1(event)
    p.digital_write(1, 0)
    p.digital_write(0, 1)
    time.sleep(2)
    releaseall(event)


def releaseall(event):
    toggle_led2(event)
    p.digital_write(1, 0)
    p.digital_write(0, 0)


def shutdownall(event):
    call("sudo shutdown -h now", shell=True)


def main():
    p.init()  # initialize the digitalio class

    pifacedigital = p.PiFaceDigital()  # instantiate
    listener = p.InputEventListener(chip=pifacedigital)

    # listener for button 0
    listener.register(0, p.IODIR_FALLING_EDGE, open)
    # listener for buutton 1
    listener.register(1, p.IODIR_FALLING_EDGE, close)
    # listener for button 2
    listener.register(2, p.IODIR_FALLING_EDGE, releaseall)
    # listener for button 3
    listener.register(3, p.IODIR_FALLING_EDGE, shutdownall)

    listener.activate()


if __name__ == "__main__":
    # calling main function
    main()
