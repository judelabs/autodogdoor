# AUTO DOG DOOR
The scripts contained in this repo of for the auto dog door project by Judelabs.  It consists of two main scripts.  One to perform the door opening via PiFace buttons,  or control via MQTT.

## PRE REQS
Make sure you enable SPI on the RPi

Ues the pip package manager to install the following:
```bash
sudo apt-get install time subprocess git python3-pip paho-mqtt
```

Then get the digitalio library for python, if using the piface:
```bash
sudo pip3 install pifacecommon
sudo pip3 install pifacedigitalio
```

## USAGE
### In the configure header of the manage_openclose.py configure your broker address, port, name and topic. Save the script.
```python
# ########CONFIGURE############
client_address = "192.168.1.2"
client_port = 1883
client_name = "dogdoormqtt"
topic_name = "dogdoor/input"


# #############################

```

### Add the scripts to CRON
```bash
su root
chmod 777 manage_openclose.py
chmod 777 dogdoorbutton.py
crontab -e
```

### In CRON you want to set up each to start at bootup.   Here is an example:
```bash
# m h  dom mon dow   command
@reboot python /home/pi/autodogdoor/dogdoorbutton.py
@reboot python /home/pi/autodogdoor/manage_openclose.py
```

### Send a message to the PI
```
"open"  - This will cause the Actuator to Close, opening the door
"close" - This will cause the Actuator to Open,  locking the door
"off"   - This cuts the power for the relays.
"shutdownpi" - This will send a shutdown message to the pi.
```

### Example Node Red Flow Simple
```
[
    {
        "id": "36c03870.f1dcf",
        "type": "tab",
        "label": "DOG DOOR",
        "disabled": false,
        "info": ""
    },
    {
        "id": "ac8b8deb.662bc8",
        "type": "inject",
        "z": "36c03870.f1dcf",
        "name": "",
        "props": [
            {
                "p": "payload"
            }
        ],
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "topic": "",
        "payload": "open",
        "payloadType": "str",
        "x": 250,
        "y": 260,
        "wires": [
            [
                "9b348f6a.9a3918"
            ]
        ]
    },
    {
        "id": "f65c9c46.bbb46",
        "type": "mqtt out",
        "z": "36c03870.f1dcf",
        "name": "",
        "topic": "dogdoor/input",
        "qos": "",
        "retain": "",
        "broker": "56b176c1.fd1048",
        "x": 650,
        "y": 320,
        "wires": []
    },
    {
        "id": "126fc9ef.054a6e",
        "type": "inject",
        "z": "36c03870.f1dcf",
        "name": "",
        "props": [
            {
                "p": "payload"
            }
        ],
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "topic": "",
        "payload": "close",
        "payloadType": "str",
        "x": 250,
        "y": 360,
        "wires": [
            [
                "b5dab106.ff69c"
            ]
        ]
    },
    {
        "id": "57e27520.91c0dc",
        "type": "inject",
        "z": "36c03870.f1dcf",
        "name": "",
        "props": [
            {
                "p": "payload"
            }
        ],
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "topic": "",
        "payload": "off",
        "payloadType": "str",
        "x": 210,
        "y": 420,
        "wires": [
            [
                "2581efa9.1a5a48"
            ]
        ]
    },
    {
        "id": "c7f9b97e.a6ac38",
        "type": "inject",
        "z": "36c03870.f1dcf",
        "name": "",
        "props": [
            {
                "p": "payload"
            }
        ],
        "repeat": "",
        "crontab": "",
        "once": false,
        "onceDelay": 0.1,
        "topic": "",
        "payload": "shutdownpi",
        "payloadType": "str",
        "x": 250,
        "y": 480,
        "wires": [
            [
                "c3cc50c4.b4c11"
            ]
        ]
    },
    {
        "id": "9b348f6a.9a3918",
        "type": "ui_button",
        "z": "36c03870.f1dcf",
        "name": "",
        "group": "7bb591d5.913d48",
        "order": 0,
        "width": 0,
        "height": 0,
        "passthru": true,
        "label": "open",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "open",
        "payloadType": "str",
        "topic": "",
        "x": 380,
        "y": 260,
        "wires": [
            [
                "f65c9c46.bbb46"
            ]
        ]
    },
    {
        "id": "b5dab106.ff69c",
        "type": "ui_button",
        "z": "36c03870.f1dcf",
        "name": "",
        "group": "7bb591d5.913d48",
        "order": 0,
        "width": 0,
        "height": 0,
        "passthru": true,
        "label": "close",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "close",
        "payloadType": "str",
        "topic": "",
        "x": 390,
        "y": 320,
        "wires": [
            [
                "f65c9c46.bbb46"
            ]
        ]
    },
    {
        "id": "2581efa9.1a5a48",
        "type": "ui_button",
        "z": "36c03870.f1dcf",
        "name": "",
        "group": "7bb591d5.913d48",
        "order": 0,
        "width": 0,
        "height": 0,
        "passthru": true,
        "label": "off",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "off",
        "payloadType": "str",
        "topic": "",
        "x": 390,
        "y": 400,
        "wires": [
            [
                "f65c9c46.bbb46"
            ]
        ]
    },
    {
        "id": "c3cc50c4.b4c11",
        "type": "ui_button",
        "z": "36c03870.f1dcf",
        "name": "",
        "group": "7bb591d5.913d48",
        "order": 0,
        "width": 0,
        "height": 0,
        "passthru": true,
        "label": "Pi Shutdown",
        "tooltip": "",
        "color": "",
        "bgcolor": "",
        "icon": "",
        "payload": "shutdownpi",
        "payloadType": "str",
        "topic": "",
        "x": 430,
        "y": 480,
        "wires": [
            [
                "f65c9c46.bbb46"
            ]
        ]
    },
    {
        "id": "56b176c1.fd1048",
        "type": "mqtt-broker",
        "z": "",
        "name": "BROKER NAME",
        "broker": "192.168.1.2",
        "port": "1883",
        "clientid": "",
        "usetls": false,
        "compatmode": true,
        "keepalive": "60",
        "cleansession": true,
        "birthTopic": "",
        "birthQos": "0",
        "birthPayload": "",
        "closeTopic": "",
        "closeQos": "0",
        "closePayload": "",
        "willTopic": "",
        "willQos": "0",
        "willPayload": ""
    },
    {
        "id": "7bb591d5.913d48",
        "type": "ui_group",
        "z": "",
        "name": "Default",
        "tab": "bc77d6db.398668",
        "disp": true,
        "width": "6",
        "collapse": false
    },
    {
        "id": "bc77d6db.398668",
        "type": "ui_tab",
        "z": "",
        "name": "DOG DOOR",
        "icon": "dashboard",
        "disabled": false,
        "hidden": false
    }
]
```