
import paho.mqtt.client as mqtt
import pifacedigitalio as p
from subprocess import call
import time

# ########CONFIGURE############
client_address = "192.168.1.26"
client_port = 1883
client_name = "dogdoormqtt"
topic_name = "dogdoor/input"


# #############################
# The callback for when the client connects to the broker
def on_connect(client, userdata, flags, rc):
    # Print result of connection attempt
    print("Connected with result code {0}".format(str(rc)))
    client.subscribe(topic_name)

def releaseall():
    p.digital_write(1, 0)
    p.digital_write(0, 0)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    # Print a received msg
    print("Message received-> " + msg.topic + " " + str(msg.payload))
    print(">" + str(msg.payload) + "<")
    if "close" in str(msg.payload):
        print("CLOSE")
        p.digital_write(1, 0)
        p.digital_write(0, 1)
        time.sleep(2)
        releaseall()

    elif "open" in str(msg.payload):
        print("OPEN")
        p.digital_write(1, 1)
        p.digital_write(0, 0)
        time.sleep(2)
        releaseall()

    elif "shutdownpi" in str(msg.payload):
        call("sudo shutdown -h now", shell=True)
    else:
        print("ELSE")
        releaseall()

def main():
    p.init()
    pifacedigital = p.PiFaceDigital()  # instantiate

#    p.digital_write(1, 0)
#    p.digital_write(0, 1)
#    time.sleep(2)
#    p.digital_write(1, 1)
#    p.digital_write(0, 0)
    time.sleep(10) #WAIT FOR NETWORK TO START

    client = mqtt.Client(client_name)
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect(client_address, client_port)
    client.loop_forever()


if __name__ == "__main__":
    # calling main function
    main()
